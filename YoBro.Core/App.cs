﻿using System;
using System.Collections.Generic;
using System.Text;
using MvvmCross;
using MvvmCross.ViewModels;
using YoBro.Core.ViewModels;

using MvvmCross.IoC;

namespace YoBro.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {

            RegisterAppStart<ViewModel>();
        }
    }
}
