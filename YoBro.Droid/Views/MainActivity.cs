﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Facebook;
using Android.Gms.Tasks;
using Firebase;
using Firebase.Auth;
using Xamarin.Facebook.Login.Widget;
using Xamarin.Facebook.Login;

[assembly: Permission(Name = Android.Manifest.Permission.Internet)]
[assembly: Permission(Name = Android.Manifest.Permission.WriteExternalStorage)]
[assembly: MetaData("com.facebook.sdk.ApplicationId", Value = "@string/facebook_app_id")]
[assembly: MetaData("com.facebook.sdk.ApplicationName", Value = "@string/app_name")]
namespace YoBro.Droid.Views
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : Activity, IFacebookCallback, IOnCompleteListener
    {
        private ICallbackManager mCallbackManager;
        private FirebaseAuth mAuth;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Create your application here
            SetContentView(Resource.Layout.activity_main);
            FirebaseApp.InitializeApp(this);
            mAuth = FirebaseAuth.Instance;
            LoginButton fblogin = FindViewById<LoginButton>(Resource.Id.fblogin);
            fblogin.SetReadPermissions(new List<string> { "email", "public_profile" });
            fblogin.Click += delegate
            {
                mCallbackManager = CallbackManagerFactory.Create();
                fblogin.RegisterCallback(mCallbackManager, this);
            };
        }

        void AuthStateChanged(object sender, FirebaseAuth.AuthStateEventArgs e)
        {
            var user = e.Auth.CurrentUser;
            if (user != null)
            {     
                // User is signed in
            }
            else
            {
                // User is signed out
            }
        }

        protected override void OnStart()
        {
            base.OnStart();
            mAuth.AuthState += AuthStateChanged;
        }

        protected override void OnStop()
        {
            base.OnStop();
            mAuth.AuthState -= AuthStateChanged;
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            var resultCodeNum = 0;
            switch (resultCode)
            {
                case Result.Ok:
                    resultCodeNum = -1;
                    break;

                case Result.Canceled:
                    resultCodeNum = 0;
                    break;

                case Result.FirstUser:
                    resultCodeNum = 1;
                    break;
            }
            mCallbackManager.OnActivityResult(requestCode, resultCodeNum, data);
        }

        private void handleFacebookAccessToken(AccessToken accessToken)
        {
            AuthCredential credential = FacebookAuthProvider.GetCredential(accessToken.Token);
            mAuth.SignInWithCredential(credential).AddOnCompleteListener(this, this);
        }

        public void OnCancel()
        {

        }

        public void OnError(FacebookException error)
        {
            throw new NotImplementedException();
        }

        public void OnSuccess(Java.Lang.Object result)
        {
            LoginResult loginResult = result as LoginResult;
            handleFacebookAccessToken(loginResult.AccessToken);
        }

        public void OnComplete(Android.Gms.Tasks.Task task)
        {
            if (task.IsSuccessful)
            {
                FirebaseUser user = mAuth.CurrentUser;
                Toast.MakeText(this, "Logged In.", ToastLength.Short).Show();
            }
            else
            {
                Toast.MakeText(this, "Authentication failed.", ToastLength.Short).Show();
            }
        }
    }
}