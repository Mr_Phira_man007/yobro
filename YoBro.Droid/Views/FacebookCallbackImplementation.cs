﻿using Android.Runtime;
using System;
using Xamarin.Facebook;

namespace YoBro.Droid.Views
{
    class FacebookCallbackImplementation<LoginResult> : Java.Lang.Object, IFacebookCallback where LoginResult : Java.Lang.Object
    {
       
        public Action HandleCancel { get; set; }
        public Action HandleError { get; set; }
        public Action HandleSuccess { get; set; }
 
        public void OnSuccess(Java.Lang.Object obj)
        {
            if (AccessToken.CurrentAccessToken != null)
            {
                var profile = Profile.CurrentProfile;
            }
        }

        public void OnCancel() { }

        public void OnError(FacebookException fbException) { }
    }
}